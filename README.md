# Studio Hyperdrive Prettier config

## Installation

```sh
$ npm install --save-dev @studiohyperdrive/prettier-config
```

Then, add a `prettier.config.js`-file where you include `"@studiohyperdrive/prettier-config"`.

```js
module.exports = require("@studiohyperdrive/prettier-config");
```

If you would like to add local overrides to the configuration:

```js
module.exports = Object.assign(
  require("@studiohyperdrive/prettier-config"),
  {
    //add overrides here
    singleQuote: true,
    bracketSpacing: true,
    ...
  }
);
```

## Integration with ESLint

We follow the guidelines defined by Prettier here: [Use ESLint to run Prettier](https://prettier.io/docs/en/eslint.html).

You need to install the following packages to integrate Pretter with Eslint:

-   [prettier](https://www.npmjs.com/package/prettier)
-   [eslint-plugin-prettier](https://www.npmjs.com/package/eslint-plugin-prettier)
-   [eslint-config-prettier](https://www.npmjs.com/package/eslint-config-prettier)

```sh
$ npm install --save-dev prettier eslint-plugin-prettier eslint-config-prettier
```

In your Eslint configuration file, extend `"plugin:prettier/recommended"` before your other rulesets.

```js
extends: [
    "plugin:prettier/recommended",
    "@studiohyperdrive/eslint-config",
    "@studiohyperdrive/eslint-config/lib/es6.js",
]
```

When setting up your npm-scripts, you could add an extra `format`-script that runs Eslint with an extra `--fix` parameter.

```js
"lint": "eslint . --ext js",
"format": "npm run lint -- --fix",
```

## Automatic formatting with common editors

By default, you don't need an editor-specific package/extension/plugin to run Prettier, just use the npm scripts provided in the project.

If you would like to run the formatter to run automatically "on save", you could use the following guides:

### Visual Studio Code

You don't need an additional Visual Studio Code extension to run Prettier, you can just enable `autoFixOnSave` from the [vscode-eslint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint).

```js
"eslint.autoFixOnSave": true
```
